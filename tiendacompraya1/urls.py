"""tiendacompraya1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from tiendacompraya1App import views
from rest_framework_simplejwt.views import (TokenObtainPairView,TokenRefreshView)

from tiendacompraya1App.views.agregarProductoView import modificar_producto

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('product/', views.ProductView.as_view()),
    path('product/agregar-producto/', views.Agregar_producto, name='agregar_producto'),
    path('product/view/', views.ProductDetailView.as_view()),
    path('product/delete/<str:name>/', views.productoEliminar.as_view()),
    path('product/update', views.ProductActualizar.as_view()),
    path('product/listar_producto/', views.agregarProductoView.listar_productos, name='listar_productos'),
    path('product/moficar_producto/<id>/', views.agregarProductoView.modificar_producto, name='modificar_producto'),
    path('product/eliminar-producto/<id>', views.agregarProductoView.eliminar_producto,name='eliminar_producto'),
]
