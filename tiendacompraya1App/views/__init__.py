from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .productView import ProductView
from .productDetailView import ProductDetailView
from .productosDeleteView import productoEliminar
from .productUpdateView import ProductActualizar
from .agregarProductoView import Agregar_producto