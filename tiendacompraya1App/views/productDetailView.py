from rest_framework import  views
from rest_framework.response import Response
from tiendacompraya1App.models.productos import Product
from tiendacompraya1App.serializers.productosSerializer import productosSerializer

class ProductDetailView(views.APIView):
    def get(self, request):
        product= Product.objects.all()
        product_serializer=productosSerializer(product,many=True)
        return Response (product_serializer.data)
