from django.http import request
from django.shortcuts import redirect, render
from rest_framework.generics import get_object_or_404
from tiendacompraya1App.models import Product, productos
from tiendacompraya1App.forms import ProductoForm
from tiendacompraya1App.views import productView

def Agregar_producto(request):

    data = {
        'form': ProductoForm()
    }

    if request.method == 'POST':
        Formulario = ProductoForm(data=request.POST, files=request.FILES)
        if Formulario.is_valid():
            Formulario.save()
            data["mensaje"] = "guardado correctamente"
        else:
            data ["form"] = Formulario

    return render (request, 'app/producto/agregar.html',data)

def listar_productos (request):
    
    productos = Product.objects.all()
    data = {
        'productos': productos
    }
    return render (request, 'app/producto/listar.html')

def modificar_producto (request, id ): 
    producto = get_object_or_404(Product, id=id)
    data = {
        'form': ProductoForm(instance=producto)
            }
    if request.method == 'POST':
        formulario = ProductoForm (data= request.Post,instance=producto, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            return redirect(to='listar_productos')
        return render (request, 'app/producto/modificar.html', data )


def eliminar_producto (request, id ): 
    producto = get_object_or_404(Product, id=id)
    producto.delete()
    return redirect (to='listar_productos')   