from django.contrib.messages.views import SuccessMessageMixin
from django import forms
from tiendacompraya1App.models.productos import Product
from django.views.generic.edit import DeleteView
from django.contrib import messages 
from django.urls import reverse

class productoEliminar(SuccessMessageMixin, DeleteView): 
    models = Product 
    forms =  Product
    fields = "__all__"     
 
 
    # Redireccionamos a la página principal luego de eliminar un registro o producto
    def get_success_url(self): 
        success_message = 'Producto Eliminado Correctamente !' # Mostramos este Mensaje luego de Editar un Producto
        messages.success (self.request, (success_message))       
        return reverse('leer') # Redireccionamos a la vista principal 'leer'