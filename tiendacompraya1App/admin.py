

from django.contrib import admin
from .models.user import User
from .models.productos import Product


admin.site.register(User)
admin.site.register(Product)


# Register your models here.