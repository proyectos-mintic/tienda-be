from tiendacompraya1App.models.productos import Product
from rest_framework import serializers

class productosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['name', 'price', 'available']