from rest_framework import serializers
#from tiendacompraya1App.models.productos import Product
from tiendacompraya1App.models.user import User
#from tiendacompraya1App.serializers.productosSerializer import productosSerializer

class UserSerializer(serializers.ModelSerializer):
    #Product = productosSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email' ]
    
    def create(self, validated_data):
        #productosData = validated_data.pop('productos')
        userInstance = User.objects.create(**validated_data)
        #Product.objects.create( **productosData)
        return userInstance
    
    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'email': user.email,
            
            
        }