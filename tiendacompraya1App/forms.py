from django import forms
from django.forms import fields
from .models import Product


class ProductoForm (forms.ModelForm):

    class Meta: 
        model = Product
        fields = '__all__'